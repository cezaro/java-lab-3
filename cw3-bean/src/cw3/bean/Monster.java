package cw3.bean;

import java.beans.*;
import java.util.ArrayList;

public class Monster implements java.io.Serializable {
    // prosta
    private String name = "Potwór";

    // ograniczona, wiązana
    private Float health = 100.0f;

    private PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private VetoableChangeSupport vetoes = new VetoableChangeSupport(this);

    public Monster() {}

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }

    public void addVetoableChangeListener (VetoableChangeListener v) {
        vetoes.addVetoableChangeListener (v);
    }

    public void removeVetoableChangeListener (VetoableChangeListener v) {
        vetoes.removeVetoableChangeListener (v);
    }

    public void setHealth(Float health) throws PropertyVetoException {
        Float oldHealth = this.health;

        vetoes.fireVetoableChange ("health", oldHealth, health);

        this.health = health;

        changes.firePropertyChange ("health", oldHealth, health);
    }

    public Float getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
