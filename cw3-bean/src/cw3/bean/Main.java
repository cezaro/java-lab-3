package cw3.bean;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

public class Main {

    public static void main(String[] args) throws PropertyVetoException {
        Monster e = new Monster();


        e.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.println("zmiana");
            }
        });

        e.setHealth(e.getHealth() - 39.0f);
    }

}

class PropertyChangeAdapter implements PropertyChangeListener {
    public void propertyChange(PropertyChangeEvent e)
    {
        float cos=(float)e.getNewValue();
        System.out.println("zmiana");
    }
}
