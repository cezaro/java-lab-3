package cw3.bean;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

public class MonsterBeanInfo extends SimpleBeanInfo {
    public PropertyDescriptor[] getPropertyDescriptors() {
        try {
            PropertyDescriptor health = new PropertyDescriptor("health", Monster.class);

            return new PropertyDescriptor [] { health };
        }
        catch (IntrospectionException e) {
            return null;
        }
    }
}
