package cw3;

import cw3.bean.Monster;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class ViewController implements Initializable {
    @FXML private Text historyText;
    @FXML private Text monsterName;
    @FXML private Text healthText;

    @FXML private Button attackBtn;

    @FXML private Rectangle healthBar;

    private ArrayList<String> history = new ArrayList<String>();

    private Monster monster = new Monster();

    public ViewController() {
        monster.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

            }
        });

        monster.addVetoableChangeListener(new VetoableChangeListener() {
            @Override
            public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
                updateHealthBar();
                generateHistoryText();

                if((Float) evt.getNewValue() <= 0) {
                    history.add("Zabity");
                    attackBtn.setDisable(true);
                    throw new PropertyVetoException("0", evt);
                }
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        monsterName.setText(monster.getName());
    }

    public void attack() throws PropertyVetoException {
        Random rnd = new Random();

        Integer chance = rnd.nextInt(100) + 1;

        if(chance <= 33)
        {
            history.add("Pudło");
        }
        else
        {
            Float dmg = Math.abs(rnd.nextFloat() * 100 / 3);
            history.add("Obrażenia: " + dmg);

            monster.setHealth(monster.getHealth() - dmg);
        }

        generateHistoryText();
    }

    private void generateHistoryText() {
        String historyString = "";
        for(String s : history)
        {
            historyString = historyString + s + "\n";
        }

        historyText.setText(historyString);
    }

    private void updateHealthBar() {
        Float originalWidth = 307.0f;
        Float health = monster.getHealth();

        healthText.setText(String.format("%.2f", health) + "%");
        healthBar.setWidth(originalWidth * health / 100);

        if(health < 50 && health >= 20) // yellow
        {
            healthBar.setFill(new Color(0.8, 0.6, 0, 1.0));
        }
        else if(health < 20) // red
        {
            healthBar.setFill(new Color(0.8, 0, 0, 1.0));
        }
    }
}
